from airflow.operators.email_operator import EmailOperator
from airflow.hooks.mssql_hook import MsSqlHook
from airflow.operators.mssql_operator import MsSqlOperator
from airflow.contrib.hooks.snowflake_hook import SnowflakeHook
from airflow.hooks.base_hook import BaseHook
from airflow.hooks.S3_hook import S3Hook
import snowflake.connector
import configparser
from datetime import datetime
import os
from jinja2 import Template
import pandas as pd
import zipfile
from zipfile import *
import s3fs
import boto3
import gzip
from sqlalchemy import create_engine
import pymssql
from snowflake.sqlalchemy import URL


def load_staging_table_qc(sql_file, table, is_incremental):
    csv_file_1 =  export_data_from_cas(sql_file, table, is_incremental)
    print(type(csv_file_1))
    print(csv_file_1)
    count = count_rows_cas(csv_file_1)
    print(count)
    return count


def export_data_from_cas(sql_file, table, is_incremental):
    connection = BaseHook.get_connection("mssql_cas3_prod")
    cas_server = connection.host
    cas_user = connection.login
    cas_password = connection.password
    cas_database = 'unicas_ux'

    base_folder = "/home/liaison/airflow"
    output_file = "{}/data/{}.txt".format(base_folder, table)

    sql_file = "{}/edw_scripts/staging/{}".format(base_folder, sql_file)
    with open(sql_file) as f:
        query_template = f.read()

    start_date = get_date_from_processloadlog()
    if len(start_date) > 23:
        start_date=start_date[:23]
    query = Template(query_template).render(is_incremental=is_incremental, default_date=start_date)
    cas_cmd = """bcp "{}" queryout {} -c -S {} -U {} -P {} -d {} """.format(query, output_file, cas_server, cas_user, cas_password, cas_database)
    print(cas_cmd)
    os.system(cas_cmd)
    return output_file

def get_date_from_processloadlog():
    base_folder = "/home/liaison/airflow"
    sql_file = "{}/edw_scripts/staging/ProcessLoadLog.sql".format(base_folder)
    with open(sql_file) as f:
        query = f.read()
    print(query)
    conn = get_snowflake_connection(schema='DBO')
    cs = conn.cursor()
    try:
        cs.execute(query)
        start_date = cs.fetchone()
    finally:
        cs.close()
    conn.close()
    return str(start_date[0])


def get_snowflake_connection(warehouse='LOADING', schema='LIA', database='EDW_STAGING', role='LOADER', autocommit=True):
    snowflake_username = BaseHook.get_connection('snowflake_loader').login
    snowflake_password = BaseHook.get_connection('snowflake_loader').password

    return snowflake.connector.connect(
        user=snowflake_username,
        password=snowflake_password,
        warehouse=warehouse,
        schema=schema,
        database=database,
        role=role,
        autocommit=autocommit,
        account='cz10842.us-east-1'
        )

def count_rows_cas(file):
    ''' Counts the number of rows of table from cas database '''
    print(type(file))
    print(file)
    num_lines = sum(1 for line in open(file))
    #data = pd.read_csv(file, sep = '\t',header=0,na_filter=False)
    #return data.shape[0],
    return num_lines


def get_rows_sql(tablename):
    ''' Counts the number of rows of table from sqlserver '''
    #base_path = '/usr/local/docs/data/ssn'
    #connection = BaseHook.get_connection("EDW_STAGING")
    server = "edw-int04.cv0qtgzdnamp.us-east-1.rds.amazonaws.com"
    user = "tmc"
    password = "Liaison1#"
    db = "EDW_STAGING"

    conn =  pymssql.connect(server, user, password, db)
    cursor = conn.cursor()
    SQLCommand = pd.read_sql_query("SELECT * FROM {}".format(tablename),conn)
    df = pd.DataFrame(SQLCommand)
    value = df.shape[0]+ 1
    return value
    conn.close()

def get_rows_snowflake(tablename, warehouse='TRANSFORM', role='TRANSFORMER'):
    ''' Counts the number of rows of table from snowflake database '''
    conn = get_snowflake_connection(schema = 'LIA',database = 'EDW_STAGING', warehouse=warehouse, role=role, autocommit=False)
    query_insert = "SELECT COUNT(*) FROM {}".format(tablename)
    cs = conn.cursor()
    cs.execute(query_insert)
    value = cs.fetchone()
    conn.close()
    return value[0]

def get_rows_S3(tablename):
    ''' Counts the number of rows of table from S3 '''
    aws_conn = BaseHook.get_connection("aws_default")
    s3_hook = S3Hook(aws_conn_id='aws_default')
    bucket = 'snowflake-liaison'
    path = "zip/{}.txt.gz".format(tablename)
    s3 = s3fs.S3FileSystem(anon=False)
    

    with s3.open(os.path.join(bucket,path), 'rb') as f:
        count = sum(1 for line in gzip.open(f,'r'))
                
    return count

def loadQC_table_to_snowflake(conn,df):
    
    conn=get_snowflake_connection(schema= "DBO", warehouse="TRANSFORM",role = "TRANSFORMER", autocommit=True)
    cursor = conn.cursor()
    # commit = "COMMIT;"
    for i in range(0,len(df)):
        query_insert = "INSERT INTO EDW_STAGING.dbo.QC_table1 SELECT %s,%s,%s,%s,%s,%s,%s,%s,%s,CURRENT_TIMESTAMP(3)::timestamp_ntz" % (df['ProcessID'][i],df['TableName'][i],df['DAG_Name'][i],df['Rows_SQL'][i],df['Rows_CAS'][i],df['Rows_S3'][i],df['Rows_Snowflake'][i],df['Diff_CAS_S3'][i],df['Diff_S3_Snowflake'][i])
        print(query_insert)
        cursor.execute(query_insert)
        #cursor.execute(commit)
        #cursor.commit()

    # conn.close()

def rows_count_compare():
    ''' Create QC_Table consisting of count of all rows of the table from CAS,SQL,S3,Snowflake and uploads QC_Table to snowflake as a part of QC '''
    
    is_incremental = False
    table_list = ['lia.STGCounty_CAS','LIA.STGAssociationInstance_CAS','LIA.STGProgram','LIA.STGStartTerm_CAS','LIA.STGApplicant_CAS','lia.STGApplicantEthnicityDetails_CAS','lia.ODSCitizenshipStatus_CAS','lia.STGVisaType_CAS','LIA.STGProgram']
    sql_list = ['STGCounty_CAS','STGAssociationInstance_CAS','STGProgram','STGStartTerm_CAS','STGApplicant_CAS','STGApplicantEthnicityDetails_CAS','ODSCitizenshipStatus_CAS','STGVisaType_CAS','STGProgram']

    df = pd.DataFrame(columns=['ProcessID','TableName','DAG_Name','Rows_SQL','Rows_CAS','Rows_S3','Rows_Snowflake','Date'])

    for i in range(0,len(table_list)):
        df = df.append({'ProcessID':"'{}'".format('TEST1'),'TableName':"'{}'".format(table_list[i]), 'DAG_Name':"'{}'".format('mathco_casapp_view'),'Rows_SQL': get_rows_sql(table_list[i]),'Rows_CAS':load_staging_table_qc(sql_list[i]+'.sql',table_list[i],is_incremental),'Rows_S3': get_rows_S3(table_list[i]),'Rows_Snowflake':get_rows_snowflake(table_list[i])},ignore_index = True)
    
    df['Diff_CAS_S3'] = df['Rows_S3'] - df['Rows_CAS']
    df['Diff_S3_Snowflake'] = df['Rows_Snowflake'] - df['Rows_S3']
    
    print('execution successful')
    loadQC_table_to_snowflake(df)
    return df


count_compare_df = rows_count_compare()
print count_compare_df
