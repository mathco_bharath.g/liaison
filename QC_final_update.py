from airflow.operators.email_operator import EmailOperator
from airflow.hooks.mssql_hook import MsSqlHook
from airflow.operators.mssql_operator import MsSqlOperator
from airflow.contrib.hooks.snowflake_hook import SnowflakeHook
from airflow.hooks.base_hook import BaseHook
from airflow.hooks.S3_hook import S3Hook
import snowflake.connector
import configparser
from datetime import datetime
import os
from jinja2 import Template
import pandas as pd
import zipfile
from zipfile import *
import s3fs
import boto3
import gzip
from sqlalchemy import create_engine
import pymssql
from snowflake.sqlalchemy import URL

def get_snowflake_connection(warehouse='LOADING', schema='LIA', database='EDW_STAGING', role='LOADER', autocommit=True):
    snowflake_username = BaseHook.get_connection('snowflake_loader').login
    snowflake_password = BaseHook.get_connection('snowflake_loader').password

    return snowflake.connector.connect(
        user=snowflake_username,
        password=snowflake_password,
        warehouse=warehouse,
        schema=schema,
        database=database,
        role=role,
        autocommit=autocommit,
        account='cz10842.us-east-1'
        )

def get_rows_snowflake(tablename, warehouse='TRANSFORM', role='TRANSFORMER'):
    ''' Counts the number of rows of table from snowflake database '''
    conn = get_snowflake_connection(schema = 'LIA',database = 'EDW_STAGING', warehouse=warehouse, role=role, autocommit=False)
    query_insert = "SELECT COUNT(*) FROM {}".format(tablename)
    cs = conn.cursor()
    cs.execute(query_insert)
    value = cs.fetchone()
    conn.close()
    return value[0]

def get_rows_S3(tablename):
    ''' Counts the number of rows of table from S3 '''
    aws_conn = BaseHook.get_connection("aws_default")
    s3_hook = S3Hook(aws_conn_id='aws_default')
    bucket = 'snowflake-liaison'
    # path = "zip/{}.txt.gz".format(tablename)
    path = "full_load/{}.txt.gz".format(tablename)
    s3 = s3fs.S3FileSystem(anon=False)
    

    with s3.open(os.path.join(bucket,path), 'rb') as filename:

        with gzip.open(filename,'rb') as f:
            file_content = f.read()
            print(type(file_content))
            file = file_content.decode("utf-8")

    print(type(file))
    row_count = file.count('|')
    return row_count



    return count

def rows_count_compare():
    ''' Create QC_Table consisting of count of all rows of the table from CAS,SQL,S3,Snowflake and uploads QC_Table to snowflake as a part of QC '''
    
    is_incremental = False
    table_list = ['lia.STGCounty_CAS','LIA.STGAssociationInstance_CAS','LIA.STGProgram','LIA.STGStartTerm_CAS','LIA.STGApplicant_CAS','lia.STGApplicantEthnicityDetails_CAS','lia.ODSCitizenshipStatus_CAS','lia.STGVisaType_CAS', 'lia.STGApplicantDetails_CAS']
    sql_list = ['STGCounty_CAS','STGAssociationInstance_CAS','STGProgram','STGStartTerm_CAS','STGApplicant_CAS','STGApplicantEthnicityDetails_CAS','ODSCitizenshipStatus_CAS','STGVisaType_CAS','STGApplicantDetails_CAS.sql']

    df = pd.DataFrame(columns=['ProcessID','TableName','DAG_Name','Rows_S3','Rows_Snowflake'])

    for i in range(0,len(table_list)):
        df = df.append({'ProcessID':"'{}'".format('TEST1'),'TableName':"'{}'".format(table_list[i]), 'DAG_Name':"'{}'".format('mathco_casapp_view'),'Rows_S3': get_rows_S3(table_list[i]),'Rows_Snowflake':get_rows_snowflake(table_list[i])},ignore_index = True)
    
    df['Diff_S3_Snowflake'] = df['Rows_Snowflake'] - df['Rows_S3']
    
    print('execution successful')
    return df